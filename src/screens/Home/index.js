import React from "react";

import CardGrid, { Card } from "../../components/CardGrid";

import "./style.scss";

function Home() {
  return (
    <div className="home">
      <h2>
        I am a Software Developer from Kingston Jamaica. This site is a
        chronicle of my growth and development as a person and as a developer.
      </h2>
      <CardGrid>
        <Card
          title="Software"
          icon="http://via.placeholder.com/100x100"
          content="Software Projects I have worked on. Client side work in JavaScript using React. Server side using Elixir, Java and Ruby."
        />
        <Card
          title="Fitness"
          icon="http://via.placeholder.com/100x100"
          content="Follow me on my fitness journey as I attempt to transform my body through diet and exercise."
        />
        <Card
          title="Contact"
          icon="http://via.placeholder.com/100x100"
          content="Interested in getting in touch with me? Leave a message here and I'll get back to you."
        />
      </CardGrid>
    </div>
  );
}

export default Home;
