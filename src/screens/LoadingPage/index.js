import React from "react";
import Spinner from "../../components/Spinner";

import "./style.scss";

function LoadingPage() {
  return (
    <div className="loading-page">
      <Spinner />
    </div>
  );
}

export default LoadingPage;
