import React from "react";
import { render } from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { withRouter } from "react-router";
import Loadable from "react-loadable";

import Layout from "./layout";
import LoadingPage from "./screens/LoadingPage";

import "bulma/css/bulma.css";

function App() {
  const LayoutWithRouter = withRouter(Layout);

  const Home = Loadable({
    loader: () => import("./screens/Home"),
    loading: LoadingPage
  });

  return (
    <BrowserRouter>
      <LayoutWithRouter>
        <Switch>
          <Route path="/" component={Home} />
        </Switch>
      </LayoutWithRouter>
    </BrowserRouter>
  );
}

render(<App />, document.getElementById("app"));
