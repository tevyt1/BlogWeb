import React from "react";

function Card({ title, icon, content }) {
  return (
    <div className="card">
      <img className="card-icon" src={icon} />
      <div className="card-title">{title}</div>
      <div className="card-content">{content}</div>
    </div>
  );
}

export default Card;
