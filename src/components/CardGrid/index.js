import React from "react";

import Card from "./Card";

import "./style.scss";

function CardGrid({ children }) {
  return <div className="card-grid">{children}</div>;
}

export default CardGrid;

export { Card };
