import React, { Component } from "react";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { menuExpanded: false };
  }

  renderLink = (link, currentPath) => {
    return (
      <a
        key={link.path}
        className={`navbar-item ${link.path == currentPath ? "is-active" : ""}`}
      >
        {link.linkText}
      </a>
    );
  };

  renderProfilePicture = ({ isMobile } = {}) => {
    return (
      <img
        className={`profile-picture ${
          isMobile ? "profile-picture-mobile" : ""
        } navbar-item`}
        src="http://via.placeholder.com/100x100"
      />
    );
  };

  /**
   * Returns the bulma.css className for menu items on mobile.
   * Appends is-active if the menu is expanded.
   * Returns just the base class otherwise
   */
  getMenuClass = baseClass => {
    if (this.state.menuExpanded) {
      return `${baseClass} is-active`;
    } else {
      return baseClass;
    }
  };

  toggleMenuExpanded = () => {
    this.setState({ menuExpanded: !this.state.menuExpanded });
  };

  render() {
    const { currentPath, links } = this.props;
    return (
      <nav className="header navbar">
        <div className="navbar-brand">
          {this.renderProfilePicture()}
          <a
            role="button"
            className={this.getMenuClass("navbar-burger")}
            aria-label="menu"
            aria-expanded="false"
            onClick={this.toggleMenuExpanded}
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>
        <div className={this.getMenuClass("nav navbar-menu")}>
          <div className="nav-menu navbar-end">
            {links.map(link => this.renderLink(link, currentPath))}
          </div>
        </div>
        {this.renderProfilePicture({ isMobile: true })}
      </nav>
    );
  }
}

export default Header;
