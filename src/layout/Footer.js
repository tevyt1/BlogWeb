import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGithub,
  faLinkedin,
  faTwitter
} from "@fortawesome/fontawesome-free-brands";

function Footer() {
  return (
    <footer>
      <div className="social-media-links">
        <a className="github" href="https://github.com/tevyt">
          <FontAwesomeIcon icon={faGithub} />
        </a>
        <a
          className="linkedin"
          href="https://www.linkedin.com/in/travis-smith-5828a779/"
        >
          <FontAwesomeIcon icon={faLinkedin} />
        </a>
        <a className="twitter" href="https://twitter.com/Tarv_OS">
          <FontAwesomeIcon icon={faTwitter} />
        </a>
      </div>
      <div className="has-text-centered">Created by Travis Smith.</div>
    </footer>
  );
}

export default Footer;
