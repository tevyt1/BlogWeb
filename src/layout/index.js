import React from "react";

import Footer from "./Footer";
import Header from "./Header";

import "./style.scss";

function Layout({ children, location: { pathname } }) {
  const links = [
    { linkText: "Home", path: "/" },
    { linkText: "Portfolio", path: "/portfolio" },
    { linkText: "About", path: "/about" },
    { linkText: "Fitness", path: "/fitness" },
    { linkText: "Contact", path: "/contact" }
  ];

  return (
    <div className="layout">
      <Header links={links} currentPath={pathname} />
      <div className="content">{children}</div>
      <Footer />
    </div>
  );
}

export default Layout;
